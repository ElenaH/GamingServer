### About

This assignment is an entry task to Playtech summer internship program.
For more information on the task details, please check [the instructions](https://gitlab.com/ElenaH/GamingServer/blob/master/Instructions.pdf).

### Goal
The program has a <b>client</b> part and a <b>server</b> part, and a <b>protocol</b> that regulates communication between them (requests and responses). The client is sending requests of three types with three different goals: to add a bet, to cancel an active bet, to retrieve information on all the bets that correspond to the filter requirements

The server was provided as an empty class. The aim was to add the server part to the existing project that would fit in the existing business logic, and pass the tests (give expected responses to the client test requests).
 
### Source Code & IDE Setup
The source code contains three Maven modules - protocol, testframework and server.  
* This is the direct link to  [Java classes] (https://gitlab.com/ElenaH/GamingServer/tree/master/server/src/main/java/com/playtech/intern2017/server/connection) that I had to add.

To set up the project in an IDE, import the provided folders as Maven modules. Originally, [Eclipse Mars](https://www.eclipse.org/mars/) was used.
To start the automated tests, start the main method of the TestFramework class. The last line of the output will tell you the results.

### Problems
For me, the main challenge is multithreading and synchronization. Now I am working on creating JUnit tests for the project and also rewriting one of my classes test-drivenly. I haven't commited those changes yet, as I had to delete the class in order to start :)
