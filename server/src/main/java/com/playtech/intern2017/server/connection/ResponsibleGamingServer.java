package com.playtech.intern2017.server.connection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;

public class ResponsibleGamingServer implements Runnable {
	private final int SERVER_SOCKET_PORT;
	private final int THREAD_POOL_LIMIT;
	private boolean continueRunning;
	private final static Logger SERVER_LOGGER = Logger.getLogger(ResponsibleGamingServer.class);
	public BetManager betManager;
	
	public ResponsibleGamingServer(int serverSocketPort) {
		this.SERVER_SOCKET_PORT = serverSocketPort;
		this.THREAD_POOL_LIMIT = 4; /* can't be set from outside */
		this.continueRunning = true;
		this.betManager = new BetManager();
	}

	public void start() {
		new Thread(this).start();
	}

	public void run() {
		SERVER_LOGGER.info("ResponsibleGamingServer is running.");
		ExecutorService poolForGamingServers = Executors.newFixedThreadPool(THREAD_POOL_LIMIT);
		try (ServerSocket listener = new ServerSocket(this.SERVER_SOCKET_PORT);) {
			while (continueRunning) {
				poolForGamingServers.execute(new ServerThread(listener.accept(), betManager));
			}
		} catch (Exception e) {
			SERVER_LOGGER.error("ResponsibleGamingServer runtime errors: " + e);
		}
	}

	public void close() {
		this.continueRunning = false;
	}
}

class ServerThread extends Thread {
	private Socket socket;
	private final static Logger SERVER_LOGGER = Logger.getLogger(ServerThread.class);
	private RequestHandler requestHandler; 
	
	public ServerThread(Socket socket, BetManager betManager) {
		this.socket = socket;
		this.requestHandler= new RequestHandler(betManager);
	}

	@Override
	public void run() {
		SERVER_LOGGER.info(this + ": Server Started and listening to the port " + socket.getPort());
		try (OutputStream outputStream = socket.getOutputStream();
				BufferedReader input = new BufferedReader(
						new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8))) {
			String request;
			while ((request = input.readLine()) != null) {
				requestHandler.handleRequest(outputStream, request);
			}
		} catch (Exception e) {
			SERVER_LOGGER.error(this + ": Unable to send response", e);
		}
	}
}