package com.playtech.intern2017.server.connection;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.playtech.intern2017.protocol.converter.ProtocolConversionUtils;
import com.playtech.intern2017.protocol.data.BetStatus;
import com.playtech.intern2017.protocol.data.ResponseStatus;
import com.playtech.intern2017.protocol.messages.*;
import com.playtech.intern2017.protocol.messages.add.*;
import com.playtech.intern2017.protocol.messages.cancel.*;
import com.playtech.intern2017.protocol.messages.query.*;

public class RequestHandler {
	private static final Lock lock = new ReentrantLock();
	private BetManager betManager;
	
	public RequestHandler(BetManager betManager) {
		this.betManager=betManager;
	}

	public void handleRequest(OutputStream outputStream, String requestJson) throws Exception {
		Request request = ProtocolConversionUtils.unmarshall(requestJson);
		Response response = null;
		if (request instanceof AddBetRequest) {
			response = handleAddBetRequest((AddBetRequest) request);
		} else if (request instanceof CancelBetRequest) {
			response = handleCancelBetRequest((CancelBetRequest) request);
		} else if (request instanceof QueryBetsRequest) {
			response = handleQueryBetsRequest((QueryBetsRequest) request);
		}
		outputStream.write((ProtocolConversionUtils.marshall(response) + "\n").getBytes(StandardCharsets.UTF_8));
		outputStream.flush();
	}

	private Response handleAddBetRequest(AddBetRequest request) {
		ResponseBuilder builder = new AddBetResponseBuilder(request);
		Bet bet = new Bet(request.getBetId(), request.getUserId(), request.getTimeStamp(), request.getAmount(),
				BetStatus.ACTIVE);
		lock.lock();
		try {
			builder = validateBet(bet, builder);
			/*
			 * try { Thread.sleep(5000); } catch (InterruptedException e)
			 * {System.out.println(e);}
			 */
			if (isRequestSuccessful(builder)) {
				betManager.addBet(bet);
			}
		} finally {
			lock.unlock();
		}
		Response response = (Response) builder.build();
		return response;
	}

protected ResponseBuilder validateBet(Bet bet, ResponseBuilder builder) {
		final long RG_LIMIT = 10000;
		long total;
		if (!betManager.isUserIdValid(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.USER_ID_INVALID);
		} else if (!betManager.isBetIdValid(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.BET_ID_INVALID);
		} else if (betManager.isAmountNegative(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.AMOUNT_NEGATIVE);
		} else if (!betManager.isTimeStampAfterStart(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.TIMESTAMP_BEFORE_START);
		} else if (!betManager.isTimeStampBeforeEnd(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.TIMESTAMP_IN_FUTURE);
		} else if (!betManager.isUnique(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_NONUNIQUE_MESSAGE);
			builder.setStatusMessage(ResponseStatusMessage.BET_ID_NONUNIQUE + bet.getBetId());
		} else if ((total = betManager.rgLimitExceeded(bet)) > RG_LIMIT) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_LIMIT_VIOLATION_MESSAGE);
			builder.setStatusMessage(ResponseStatusMessage.RG_LIMIT_EXCEEDED + total);
		} else {
			builder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		}
		return builder;
	}

	private Response handleCancelBetRequest(CancelBetRequest request) {
		ResponseBuilder builder = new CancelBetResponseBuilder(request);
		String betId = request.getBetId();
		lock.lock();
		try {
			Bet bet = betManager.findBetById(betId);
			builder = validateCancelation(bet, builder);
			if (isRequestSuccessful(builder)) {
				betManager.cancelBet(bet);
			}
		} finally {
			lock.unlock();
		}
		Response response = (Response) builder.build();
		return response;
	}

	private ResponseBuilder validateCancelation(Bet bet, ResponseBuilder builder) {
		if (betManager.doesntExist(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_NOT_FOUND);
			builder.setStatusMessage(ResponseStatusMessage.NOT_FOUND + bet.getBetId());
		} else if (betManager.isAlreadyCancelled(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_INVALID);
			builder.setStatusMessage(ResponseStatusMessage.ALREADY_CANCELLED + bet.getBetId());
		} else {
			builder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		}
		return builder;
	}

	private Response handleQueryBetsRequest(QueryBetsRequest request) {
		QueryBetsResponseBuilder newResponseBuilder = new QueryBetsResponseBuilder(request);
		BetFilter newFilter = request.getFilter();
		newResponseBuilder.setBets(betManager.getFilteredBets(newFilter));
		newResponseBuilder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		Response response = (Response) newResponseBuilder.build();
		return response;
	}
	
	private boolean isRequestSuccessful(ResponseBuilder builder){
		return (builder.getStatusCode() == ResponseStatus.STATUS_SUCCESS);
	}

}