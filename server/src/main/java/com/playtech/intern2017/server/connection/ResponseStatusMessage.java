package com.playtech.intern2017.server.connection;

public class ResponseStatusMessage {
/*add*/
	public final static String USER_ID_INVALID = "userId cannot be null or empty";
	public final static String BET_ID_INVALID = "betId cannot be null or empty";
	public final static String AMOUNT_NEGATIVE = "amount cannot be negative";
	public final static String TIMESTAMP_BEFORE_START = "timestamp cannot be before 1.03.2017 00:00:00.000";
	public final static String TIMESTAMP_IN_FUTURE = "timestamp cannot be in the future";
/*cancel*/
	public final static String ALREADY_CANCELLED = "Bet already cancelled: ";//+betId
	public final static String NOT_FOUND = "Bet not found: ";//+betId
	public final static String BET_ID_NONUNIQUE="Bet already present: ";//+betId
	public final static String RG_LIMIT_EXCEEDED="RG limit violation: ";//+amount
}
