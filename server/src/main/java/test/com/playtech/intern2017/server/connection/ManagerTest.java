package test.com.playtech.intern2017.server.connection;

public class ManagerTest {

	package test.com.playtech.intern2017.server.connection;

	import java.sql.Timestamp;
	import java.util.Collection;
	import java.util.TreeSet;
	import java.util.concurrent.CopyOnWriteArrayList;

	import org.junit.*;

	import com.playtech.intern2017.protocol.data.BetStatus;
	import com.playtech.intern2017.protocol.messages.query.Bet;
	import com.playtech.intern2017.protocol.messages.query.BetFilter;
	import com.playtech.intern2017.server.connection.BetManager;

	public class BetManagerTest {

		private static Collection<Bet> allBetsForTest = new CopyOnWriteArrayList<Bet>();
		private static Collection<Bet> filteredBets = new TreeSet<Bet>();
		private static BetFilter filter;
		private static Bet bet1, bet2, uniqueBet, nonuniqueBet, negativeBet, betWithNullId, betWithEmptyId, betWithIdSpacesOnly,
				betWithNullUser, betWithEmptyUser, betWithUserSpacesOnly;
		private static long minTimeStampOfDay = (long) (1519948800 * 1000); // 03/02/2018 @
																		// 12:00am
																		// (UTC)
		private static long maxTimeStampOfDay = (long) (1520035200 * 1000) - 1;
		private static long difference = (long) (152050482.278 * 1000);
		private static long middleTimeStampOfDay = (long) (1519948800 * 1000) + 9000 * 1000 * 1000;
		private static String userId = "sbvt";
		private static BetStatus active = BetStatus.ACTIVE;
		private static BetStatus cancelled = BetStatus.CANCELLED;
		private static String betId;
		private static long timeStamp;
		private static long amount;
		private static long minAmount = 1;
		private static long maxAmount = 1000;
		private static final Long START_TIMESTAMP = new Long("1488326400000");
		private static long currentTimeStamp = new Timestamp(System.currentTimeMillis()).getTime();
		private static long timeStampInFuture = currentTimeStamp + 10000;

		@Before
		public void createBets() {
			timeStamp = middleTimeStampOfDay;
			amount = 100;
			betId = null;
			betWithNullId = new Bet(betId, userId, timeStamp, amount, active);
			betId = "";
			betWithEmptyId = new Bet(betId, userId, timeStamp, amount, active);
			betId = "   ";
			betWithIdSpacesOnly = new Bet(betId, userId, timeStamp, amount, active);
			betId = "sbvt-0";
			bet1 = new Bet(betId, userId, timeStamp, amount, active);
			userId = null;
			betWithNullUser = new Bet(betId, userId, timeStamp, amount, active);
			userId = "";
			betWithEmptyUser = new Bet(betId, userId, timeStamp, amount, active);
			userId = "   ";
			betWithUserSpacesOnly = new Bet(betId, userId, timeStamp, amount, active);
			userId = "sbvt";

			betId = "sbvt-1";
			timeStamp = middleTimeStampOfDay + 1000;
			amount = 9800;
			bet2 = new Bet(betId, userId, timeStamp, amount, active);
			nonuniqueBet = bet2;
			betId = "sbvt-2";
			uniqueBet = new Bet(betId, userId, timeStamp, amount, active);
			amount = -1000;
			negativeBet = new Bet(betId, userId, timeStamp, amount, active);
		}

		@Before
		public void createExpectedBetsList() {
			allBetsForTest.add(bet1);
			allBetsForTest.add(bet2);
		}

		@Before
		public void createActualBetsList() {
			BetManager.addNewBet(bet1);
			BetManager.addNewBet(bet2);
		}

		@Before
		public void createFilter() {
			betId = null;
			filter = new BetFilter(betId, userId, minTimeStampOfDay, maxTimeStampOfDay, minAmount, maxAmount, active);
		}

		@Before
		public void createFilteredBets() {
			//filteredBets.add(bet1);
		}

		@Test
		public void testGetAllBets() {
			Assert.assertEquals(allBetsForTest, BetManager.getAllBets());
		}

		@Test
		public void testGetFilteredBets() {
			//Assert.assertEquals(filteredBets, BetManager.getFilteredBets(filter));
		}

		@Test
		public void testBetMatchesFilter_betIdsMatch(){
			betId="svb-9";
			Bet bet=new Bet("svb-9", "", 0, 0, active);
			filter = new BetFilter(betId, null, null, null, null, null, active);
			Assert.assertTrue(BetManager.betMatchesFilter(bet, filter));
			
		}
		
		
		
		
		@Test
		public void testIsBetUniqueOnUniqueBet() {
			Assert.assertTrue(BetManager.isBetUnique(uniqueBet.getBetId()));
		}

		@Test
		public void testIsBetUniqueOnNonuniqueBet() {
			Assert.assertFalse(BetManager.isBetUnique(nonuniqueBet.getBetId()));
		}

		@Test
		public void testFindBetbyId() {
			betId = bet2.getBetId();
			Assert.assertEquals(BetManager.findBetById(betId), bet2);
		}

		/*
		 * ToTest:
		 * 
		 * public static long rgLimitExceeded(Bet newBet)
		 * 
		 * public static Bet findBetById(String betId)
		 * 
		 * public static void cancelBet(Bet bet)
		 * 
		 * public static boolean isBetIdValid(Bet bet)
		 * 
		 * public static boolean isUserIdValid(Bet bet)
		 * 
		 * public static boolean isAmountNegative(Bet bet)
		 * 
		 * public static boolean isTimeTooEarly(Bet bet)
		 * 
		 * public static boolean isTimeInFuture(Bet bet)
		 */

	}

}
