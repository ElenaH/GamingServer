package test.com.playtech.intern2017.server.connection;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({BetManagerTest.class})
public class AllTests {

}
