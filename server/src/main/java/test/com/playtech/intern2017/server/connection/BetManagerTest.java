package test.com.playtech.intern2017.server.connection;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import com.playtech.intern2017.protocol.data.BetStatus;
import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.protocol.messages.query.BetFilter;
import com.playtech.intern2017.server.connection.BetManager;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class BetManagerTest {
	private static final Long START_TIMESTAMP = new Long("1488326400000");
	public static Bet bet, bet1, bet2, bet3, bet4;
	//bets for the list
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 bet=new Bet("svb-9", "svb", START_TIMESTAMP+9000, 0, BetStatus.ACTIVE);
		bet1=new Bet("svb-1", "svb", START_TIMESTAMP+10000, 1, BetStatus.ACTIVE);
		bet2=new Bet("svb-2", "svb", START_TIMESTAMP+15000, 9000, BetStatus.ACTIVE);
        bet3=new Bet("ttt-3", "ttt", START_TIMESTAMP+20000, 100, BetStatus.ACTIVE);	
        bet4=new Bet("ttt-0", "ttt", START_TIMESTAMP+16000, 100, BetStatus.CANCELLED);	
		}

	
	@Test
	public void testBetMatchesFilter_NullFilter() {		
		BetFilter nullFilter = new BetFilter(null, null, null, null, null, null, null);
		Assert.assertTrue(BetManager.betMatchesFilter(bet, nullFilter));
	}
	
	@Test
	public void testBetMatchesFilter_betIdsMatch() {		
		BetFilter filter = new BetFilter("svb-9", null, null, null, null, null, null);
		Assert.assertTrue(BetManager.betMatchesFilter(bet, filter));
	}
	
	@Test
	public void testBetMatchesFilter_betIdsDontMatch() {
		BetFilter filter = new BetFilter("svb-8", null, null, null, null, null, null);
		Assert.assertFalse(BetManager.betMatchesFilter(bet, filter));
	}
	
	@Test
	public void testBetMatchesFilter_userIdsMatch() {
		BetFilter filter = new BetFilter(null, "svb", null, null, null, null, null);
		Assert.assertTrue(BetManager.betMatchesFilter(bet, filter));
	}
	
	@Test
	public void testBetMatchesFilter_userIdsDontMatch() {
		BetFilter filter = new BetFilter(null, "brn", null, null, null, null, null);
		Assert.assertFalse(BetManager.betMatchesFilter(bet, filter));
	}
	
	@Test
	public void testBetMatchesFilter_statusesMatch() {
		BetFilter filter = new BetFilter(null, null, null, null, null, null, BetStatus.ACTIVE);
		Assert.assertTrue(BetManager.betMatchesFilter(bet, filter));
	}
	
	@Test
	public void testBetMatchesFilter_statusesDontMatch() {
		BetFilter filter = new BetFilter(null, null, null, null, null, null, BetStatus.CANCELLED);
		Assert.assertFalse(BetManager.betMatchesFilter(bet, filter));
	}
	
}
