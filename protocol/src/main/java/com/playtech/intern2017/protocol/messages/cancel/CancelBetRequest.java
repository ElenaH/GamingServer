package com.playtech.intern2017.protocol.messages.cancel;

import com.playtech.intern2017.protocol.messages.Request;

public class CancelBetRequest extends Request {
	
	private final String betId;

    protected CancelBetRequest(long messageId, String betId) {
		super(messageId);
		this.betId = betId;
	}

    public String getBetId() {
		return betId;
	}

}
