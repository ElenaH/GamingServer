package com.playtech.intern2017.protocol.messages.add;

import com.playtech.intern2017.protocol.messages.Response;

public class AddBetResponse extends Response {

    protected AddBetResponse(long messageId, int statusCode, String statusMessage) {
		super(messageId, statusCode, statusMessage);
	}

}
