package com.playtech.intern2017.protocol.messages.cancel;

import com.playtech.intern2017.protocol.messages.ResponseBuilder;

public class CancelBetResponseBuilder extends ResponseBuilder {

	public CancelBetResponseBuilder() {}
	
	public CancelBetResponseBuilder(CancelBetRequest request) {
		super(request);
	}
	
	public CancelBetResponse build() {
		return new CancelBetResponse(getMessageId(), getStatusCode(), getStatusMessage());
	}

}