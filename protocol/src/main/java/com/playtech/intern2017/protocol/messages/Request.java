package com.playtech.intern2017.protocol.messages;

public abstract class Request extends MessageBody {

	protected Request(long messageId) {
		super(messageId);
	}

}